reset

set terminal epslatex color

set xlabel "$\beta$ Werte"
set ylabel "Lyapunov-Exponent $\lambda$"

set output "layaplot1.tex"
p "layaalp5.9.dat" u 1:2 t "$\alpha=5.9$" w l, "layaalp6.2.dat" u 1:2 t "$\alpha=6.2$" w l
set output
!epstopdf layaplot1.eps

set output "layaplot2.tex"
p "layaalp5.9.dat" u 1:3 t "$\alpha=5.9$" w l, "layaalp6.2.dat" u 1:3 t "$\alpha=6.2$" w l
set output
!epstopdf layaplot2.eps
