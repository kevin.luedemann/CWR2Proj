#include <iostream>//IO Befehle
#include <cstdlib>//system() Befehl
#include <cmath>//Math biblothel
#include <fstream>//Datei schreiben
#include <string>//strings erstellen

using namespace std;
/*Unterprogramm, um dan Lyapunov-Exponente über die Definition des Exponenten.
 *Um Fehler der Rückgabe zu umgehen, stelle ich eine Abfrage, ob die n-ten Funktionswerte gleich sind, da der
 *Logarithmuss an 0 nicht definiert ist.
 *Dies ist aber nur an der Rändern, die ausgenommen sind der Fall, deswegen beinfluss diese Änderung nicht die Daten.
 * */
double laya(double x0, double xn, double y0, double yn, double n){
if(abs(xn-yn)==0){return 0;}
else{return (abs(xn-yn)/abs(x0-y0))/n;}
}

/*Unterprogramm zu berechnen des Lyapuno-Exponenten aus der Ableitung.
 *Hierzu wurde die Ableitung analytisch gebildet.
 *Es werden alle Punkte eingesetzt und aufsummiert.
 *Hierraus ergibt sich der Exponent für einen festen Startwert x[0]*/
double laya2(double x[], double alpha, double n){
double la = 0;
for(int i = 0; i <n; i++){
	la += log(abs(2*alpha*x[i]*exp(-alpha*x[i]*x[i])));
}
return la/n;
}

/*Unterprogramm, um die Bifurkation zu bestimmen.
 * Hier befinden sich alle Hauptschliefen und die Berechnung der Abbildung wird durchgeführt.
 * Zu dem wird hier auch die Berechnung der Exponenten vorgenommen.
 * Anschließend wird alles in zwei Dateien geschrieben.
 * Die Exponenten für jeden Startwert werden zu dem noch in die Datei der Bifurkation mit hinein geschrieben.
 */
void bifurk(double alpha, string name){
ofstream file(name.c_str());//Ausabe für die Bifurkation
string dat = "laya";
dat +=name;
ofstream out(dat.c_str());//Ausgabe für den Exponenten
double schrittb = 0.001;//Schrittweite für beta
double schrittx = 0.01;//Schrittweite der Startwerte
double beta = -1+schrittb;
double la = 0, la2 = 0;//Die beiden Variablen für die Exponenten nach Definition und nach Ableitung
int n = 1000; //Anzahl der Iterationschritte
double x[1010];//Variable zum Speichern der Funktionswerte
double z[1010];//Variable zum speichern der letzten Abbildung für den letzten Startwert
while(beta < 1){//Schleife zum Durchlaufen aller beta Werte
	x[0]=0;
	la=0;
	la2=0;
	while(x[0]<1){//Schleife zum durchlaufen aller Startwerte x[0]
		for(int i = 1; i <= n; i++){//Bestimmung der Abbildung für n Iterationsschritte
			x[i]=exp(-alpha*x[i-1]*x[i-1])+beta;

		}
	if(x[0]>schrittx){//Sicherheitsabfrage, damit in den unterprogrammen nicht durch 0 geteielt wird, da im ersten Schritt alle Werte gleich sind	
		la += laya(x[0],x[n],z[0],z[n],n);
		la2 += laya2(&x[0],alpha,n);
		}
		//Ausgabe der Befurktion und der einzelnen Lyapunov-Exponenten
		file << beta << " " << x[n] << " " << laya(x[0],x[n],z[0],z[n],n) << " " << laya2(&x[0],alpha,n) << endl;
		for(int i = 0; i <= n; i++){z[i]=x[i];}//Speichern der alten Werte in einem weiteren Array zur späteren Berechnung des Exponenten
		x[0] += schrittx;
	}
	cout << beta << " " << la/100 << " " << la2/100 << endl;//Ausgabe als eine Art Satusbar, so sieht man, wie weit das Programm bereits ist, da es recht lange braucht.
	//Speichern der Mittelwerte der einzelnen Exponenten
	out << beta << " " << la/100 << " " << la2/100 << endl;
	beta+=schrittb;
}
out.close();
file.close();
}

//Unterprogtramm zum erzeugen der Plot datei
void gnuplot(){
ofstream out("gnuplot.gp");//ofstream für die Gnuplotdatei
//ausgabe aller notwendigen befehle, um alle wichtigen pots zu erstellen
out << "reset" << endl;

out << "set terminal epslatex color" << endl;

out << "set xlabel '$\\beta$ Werte'" << endl;
out << "set ylabel '$x_n$ Werte für n=10000'" << endl;

out << "set output 'alpa59.tex'" << endl;
out << "p 'alp5.9.dat' u 1:2 w d t '$\\alpha = 5.9$'" << endl;
out << "set output" << endl;
out << "!epstopdf alpa59.eps" << endl;

out << "set output 'alpa62.tex'" << endl;
out << "p 'alp6.2.dat' u 1:2 w d t '$\\alpha = 6.2$'" << endl;
out << "set output" << endl;
out << "!epstopdf alpa62.eps" << endl;


out << "set ylabel 'Lyapunov-Exponent $\\lambda$'" << endl;

out << "set output 'layaplot1.tex'" << endl;
out << "p 'layaalp5.9.dat' u 1:2 t '$\\alpha=5.9$' w l, 'layaalp6.2.dat' u 1:2 t '$\\alpha=6.2$' w l" << endl;
out << "set output" << endl;
out << "!epstopdf layaplot1.eps" << endl;

out << "set output 'layaplot2.tex'" << endl;
out << "p 'layaalp5.9.dat' u 1:3 t '$\\alpha=5.9$' w l, 'layaalp6.2.dat' u 1:3 t '$\\alpha=6.2$' w l" << endl;
out << "set output" << endl;
out << "!epstopdf layaplot2.eps" << endl;

out.close();
}


int main(){

//Aufrufe der Unterprogramme zur Berechnung der Daten für beide alpha Werte
bifurk(5.9,"alp5.9.dat");
bifurk(6.2,"alp6.2.dat");


/*Erstelen der Plots.
 * Hier werden die Dateien angelegt, die die Plots nach dem durchlauf des Programms erzeugen
 */
gnuplot();
//ausführen von Gnuplot und das Erstellen der Plots
system("gnuplot gnuplot.gp");

return 0;
}
