reset

set terminal epslatex color

set xlabel "$\beta$ Werte"
set ylabel "$x_n$ Werte für n=10000"

set output "alpa59.tex"
p "alp5.9.dat" u 1:2 w d t "$\alpha = 5.9$"
set output
!epstopdf alpa59.eps

set output "alpa62.tex"
p "alp6.2.dat" u 1:2 w d t "$\alpha = 6.2$"
set output
!epstopdf alpa62.eps
